//
//  main.m
//  BarcodeGen
//
//  Created by Peerasak Unsakon on 9/29/2558 BE.
//  Copyright © 2558 Peerasak Unsakon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
