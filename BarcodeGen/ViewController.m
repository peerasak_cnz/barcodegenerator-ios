//
//  ViewController.m
//  BarcodeGen
//
//  Created by Peerasak Unsakon on 9/29/2558 BE.
//  Copyright © 2558 Peerasak Unsakon. All rights reserved.
//

#import "ViewController.h"
#import "ZXingObjC.h"

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *codeTextfield;
@property (weak, nonatomic) IBOutlet UIImageView *codeImageView;

@end

@implementation ViewController {
    ZXBarcodeFormat codeFormat;
    NSArray *allFormats;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    codeFormat = kBarcodeFormatCode128;
    [_codeTextfield setText:@"RT077777"];
    [self encodeString:_codeTextfield.text];
    
    allFormats = @[
                   @{@"name":@"kBarcodeFormatCode128",@"enum":[NSNumber numberWithInt:kBarcodeFormatCode128]},
  @{@"name":@"kBarcodeFormatCode39",@"enum":[NSNumber numberWithInt:kBarcodeFormatCode39]},
  @{@"name":@"kBarcodeFormatCodabar",@"enum":[NSNumber numberWithInt:kBarcodeFormatCodabar]},
  @{@"name":@"kBarcodeFormatEan8",@"enum":[NSNumber numberWithInt:kBarcodeFormatEan8]},
  @{@"name":@"kBarcodeFormatEan13",@"enum":[NSNumber numberWithInt:kBarcodeFormatEan13]},
  @{@"name":@"kBarcodeFormatITF",@"enum":[NSNumber numberWithInt:kBarcodeFormatITF]},
  @{@"name":@"kBarcodeFormatUPCA",@"enum":[NSNumber numberWithInt:kBarcodeFormatUPCA]},
  @{@"name":@"kBarcodeFormatUPCE",@"enum":[NSNumber numberWithInt:kBarcodeFormatUPCE]}
                   ];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    [self encodeString:_codeTextfield.text];
    return YES;
}

- (void)encodeString:(NSString *)codeString {
    
    CGFloat width = _codeImageView.bounds.size.width;
    CGFloat height = _codeImageView.bounds.size.height;
    
@try {
    NSError *error = nil;
    ZXMultiFormatWriter *writer = [ZXMultiFormatWriter writer];
    ZXBitMatrix *result = [writer encode:codeString
                                  format:codeFormat
                                   width:width
                                  height:height
                                   error:&error];
    
    if (result) {
        CGImageRef image = [[ZXImage imageWithMatrix:result] cgimage];
        [_codeImageView setImage:[UIImage imageWithCGImage:image]];
    } else {
        NSString *errorMessage = [error localizedDescription];
        NSLog(@"error: %@", errorMessage);
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Error"
                                     message:errorMessage
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }

}
@catch (NSException *exception) {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Error"
                                 message:@"It's just ERROR!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@finally {
    NSLog(@"Okay");
}
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self encodeString:_codeTextfield.text];
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)changeFormat:(id)sender {
    
    UIAlertController *alertAction =   [UIAlertController
                                 alertControllerWithTitle:@"Barcode Format"
                                 message:@"Select you format"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (NSDictionary *format in allFormats) {
        UIAlertAction *code = [UIAlertAction
                                 actionWithTitle:format[@"name"]
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     //Do some thing here
                                     codeFormat = [format[@"enum"] intValue];
                                     [self encodeString:_codeTextfield.text];
                                     [alertAction dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        [alertAction addAction:code];
    }

    [self presentViewController:alertAction animated:YES completion:nil];

}

@end
